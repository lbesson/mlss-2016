# [**MLSS 2016** : poster session](http://learning.mpi-sws.org/mlss2016/) (Cadiz, Spain)
## My poster : *« Hidden Markov Models and Hidden semi-Markov Models »*
> A 1-page A0-sized PDF poster, [written in LaTeX (LaTeX 2e)](https://bitbucket.org/lbesson/mva15-project-probabilistic-graphical-models/src/master/poster_MLSS2016/MLSS_2016__Poster_on_HMM_and_HSSM__Lilian_Besson__05-2016.en.tex).

Poster for the *junior* [poster session](http://learning.mpi-sws.org/mlss2016/poster-sessions/) (undergraduate and graduate students), at the [Machine Learning Summer School](http://mlss.cc/) [2016](http://learning.mpi-sws.org/mlss2016/) ([Cadiz, Spain](http://learning.mpi-sws.org/mlss2016/location/)), [11-05 - 21-05 2016](http://learning.mpi-sws.org/mlss2016/schedule/).

Note: [Apparently, I will present it twice](http://learning.mpi-sws.org/mlss2016/poster-sessions/), once at a joint AISTAT/MLSS poster session the first day (Wednesday 11th of May), and a second time the next week (regular slot).

[See the poster (PDF)](http://lbo.k.vu/posterMLSS2016) ([also here](https://bitbucket.org/lbesson/mva15-project-probabilistic-graphical-models/downloads/MLSS_2016__Poster_on_HMM_and_HSSM__Lilian_Besson__05-2016.en.pdf)).

### More information on [MLSS](http://mlss.cc/) [2016](http://learning.mpi-sws.org/mlss2016/) ?
> Here: [FAQ](http://learning.mpi-sws.org/mlss2016/faq/), [information about the poster sessions](http://learning.mpi-sws.org/mlss2016/faq/), [list of invited speakers](http://learning.mpi-sws.org/mlss2016/speakers/), and [planning of the conferences](http://learning.mpi-sws.org/mlss2016/schedule/).
> Many thanks to [the MLSS 2016 organizers](http://learning.mpi-sws.org/mlss2016/organizers/) !

---

## About
[This project](https://bitbucket.org/lbesson/mva15-project-probabilistic-graphical-models/) was done for the [Probabilistic Graphical Models](http://www.di.ens.fr/~slacoste/teaching/MVA_GM/fall2015/) course for the [MVA master program](http://www.cmla.ens-cachan.fr/version-anglaise/academics/mva-master-degree-227777.kjsp) at [ENS de Cachan](http://www.ens-cachan.fr).
(Valentin and I got a grade of *16/20* for our project.)

### Copyright
(C), 2015-16, [Lilian Besson](http://perso.crans.org/besson/) et [Valentin Brunck](http://thesilkmonkey.com/science/science.php) ([ENS de Cachan](http://www.ens-cachan.fr)).

### Licence
This project is publicly published, under the terms of the [MIT license](http://lbesson.mit-license.org/).
